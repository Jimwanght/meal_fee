# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181121015327) do

  create_table "academic_partition_hierarchies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "ancestor_id", null: false
    t.bigint "descendant_id", null: false
    t.integer "distance", default: 0, null: false
    t.index ["ancestor_id", "descendant_id"], name: "unique_hierarchy", unique: true
    t.index ["ancestor_id"], name: "index_academic_partition_hierarchies_on_ancestor_id"
    t.index ["descendant_id"], name: "index_academic_partition_hierarchies_on_descendant_id"
  end

  create_table "academic_partitions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "学院区划（学校、学部、年级、班级等）" do |t|
    t.string "name", limit: 30, comment: "名称"
    t.bigint "parent_id", comment: "上级学院区划ID"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type", limit: 10, null: false
    t.index ["name", "parent_id"], name: "index_academic_partitions_on_name_and_parent_id", unique: true
    t.index ["parent_id", "type"], name: "index_academic_partitions_on_parent_id_and_type"
    t.index ["parent_id"], name: "index_academic_partitions_on_parent_id"
  end

  create_table "admins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "管理员" do |t|
    t.string "username", limit: 30, null: false, comment: "用户名"
    t.string "password_digest", limit: 60, null: false, comment: "加密后的密码"
    t.boolean "root", default: false, null: false, comment: "是否超级管理员"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["username"], name: "index_admins_on_username", unique: true
  end

  create_table "charges", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "缴费种类" do |t|
    t.string "name", limit: 30, null: false, comment: "名称"
    t.decimal "fee", precision: 10, scale: 2, default: "0.0", null: false, comment: "金额"
    t.bigint "plan_id", comment: "缴费计划ID"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "plan_id"], name: "index_charges_on_name_and_plan_id", unique: true
    t.index ["plan_id"], name: "index_charges_on_plan_id"
  end

  create_table "order_histories", id: :bigint, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "payer_id", comment: "付款人ID"
    t.bigint "payment_request_id", comment: "付款请求ID"
    t.string "payment_method", limit: 6, comment: "支付方式"
    t.string "payment_username", limit: 50, comment: "支付平台的账号"
    t.string "transaction_code", limit: 50, comment: "支付平台流水号"
    t.string "state", limit: 10, default: "new", null: false, comment: "订单状态"
    t.datetime "url_generated_at", comment: "支付URL生成时间"
    t.datetime "completed_at", comment: "支付流程完成时间"
    t.string "order_num", limit: 11, null: false, comment: "订单号"
    t.string "credit_card_num", limit: 50, comment: "银行卡号"
    t.string "credit_card_holder", limit: 50, comment: "持卡人"
    t.string "bank", limit: 50, comment: "开户银行"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "charge_id", comment: "收费类型ID"
    t.string "parent_name", limit: 50, null: false, comment: "家长姓名"
    t.string "parent_mobile", limit: 11, null: false, comment: "家长手机"
    t.index ["charge_id"], name: "index_order_histories_on_charge_id"
    t.index ["order_num"], name: "index_order_histories_on_order_num", unique: true
    t.index ["payer_id"], name: "index_order_histories_on_payer_id"
    t.index ["payment_request_id"], name: "index_order_histories_on_payment_request_id"
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "payer_id", comment: "付款人ID"
    t.bigint "payment_request_id", comment: "付款请求ID"
    t.string "payment_method", limit: 6, comment: "支付方式"
    t.string "payment_username", limit: 50, comment: "支付平台的账号"
    t.string "transaction_code", limit: 50, comment: "支付平台流水号"
    t.string "state", limit: 10, default: "new", null: false, comment: "订单状态"
    t.datetime "url_generated_at", comment: "支付URL生成时间"
    t.datetime "completed_at", comment: "支付流程完成时间"
    t.string "order_num", limit: 11, null: false, comment: "订单号"
    t.string "credit_card_num", limit: 50, comment: "银行卡号"
    t.string "credit_card_holder", limit: 50, comment: "持卡人"
    t.string "bank", limit: 50, comment: "开户银行"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "charge_id", comment: "收费类型ID"
    t.string "parent_name", limit: 50, null: false, comment: "家长姓名"
    t.string "parent_mobile", limit: 11, null: false, comment: "家长手机"
    t.integer "lock_version", default: 0, null: false
    t.index ["charge_id"], name: "index_orders_on_charge_id"
    t.index ["order_num"], name: "index_orders_on_order_num", unique: true
    t.index ["payer_id"], name: "index_orders_on_payer_id"
    t.index ["payment_request_id"], name: "index_orders_on_payment_request_id", unique: true
  end

  create_table "payers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "付款人" do |t|
    t.string "mobile", limit: 11, null: false, comment: "手机号"
    t.string "password_digest", limit: 60, null: false, comment: "加密后的密码"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mobile"], name: "index_payers_on_mobile", unique: true
  end

  create_table "payment_notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.bigint "order_history_id", comment: "订单历史ID"
    t.string "payment_method", limit: 6
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "raw_content"
    t.string "request_id"
    t.index ["order_history_id"], name: "index_payment_notifications_on_order_history_id"
  end

  create_table "payment_requests", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "student_name", limit: 50, null: false, comment: "学生姓名"
    t.bigint "academic_partition_id", comment: "学生的班级"
    t.bigint "plan_id", comment: "缴费计划ID"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["academic_partition_id"], name: "index_payment_requests_on_academic_partition_id"
    t.index ["plan_id"], name: "index_payment_requests_on_plan_id"
    t.index ["student_name", "academic_partition_id", "plan_id"], name: "uniq_student_name", unique: true
  end

  create_table "plans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "缴费计划" do |t|
    t.string "name", limit: 50, null: false, comment: "计划名称"
    t.integer "payment_requests_count", default: 0, null: false, comment: "应缴人数"
    t.integer "paid_count", default: 0, null: false, comment: "实缴人数"
    t.boolean "active", comment: "是否正在缴费。是：1，否：NULL"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "protocol_title"
    t.text "protocol_content"
    t.index ["name"], name: "index_plans_on_name", unique: true
  end

  create_table "targets", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", comment: "缴费计划覆盖的院校" do |t|
    t.bigint "plan_id", comment: "缴费计划ID"
    t.bigint "academic_partition_id", comment: "院校ID"
    t.index ["academic_partition_id", "plan_id"], name: "index_targets_on_academic_partition_id_and_plan_id", unique: true
    t.index ["academic_partition_id"], name: "index_targets_on_academic_partition_id"
    t.index ["plan_id"], name: "index_targets_on_plan_id"
  end

  create_table "verification_codes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "mobile", null: false
    t.string "code", null: false
    t.datetime "expires_at", null: false
    t.string "usage", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mobile", "usage"], name: "index_verification_codes_on_mobile_and_usage", unique: true
  end

  add_foreign_key "academic_partition_hierarchies", "academic_partitions", column: "ancestor_id", on_delete: :cascade
  add_foreign_key "academic_partition_hierarchies", "academic_partitions", column: "descendant_id", on_delete: :cascade
  add_foreign_key "academic_partitions", "academic_partitions", column: "parent_id", on_delete: :cascade
  add_foreign_key "charges", "plans", on_delete: :cascade
  add_foreign_key "order_histories", "charges"
  add_foreign_key "order_histories", "payers"
  add_foreign_key "order_histories", "payment_requests"
  add_foreign_key "orders", "charges"
  add_foreign_key "orders", "payers"
  add_foreign_key "orders", "payment_requests"
  add_foreign_key "payment_notifications", "order_histories"
  add_foreign_key "payment_requests", "academic_partitions"
  add_foreign_key "payment_requests", "plans", on_delete: :cascade
  add_foreign_key "targets", "academic_partitions"
  add_foreign_key "targets", "plans", on_delete: :cascade
end
