class AddRequestIdToPaymentNotifications < ActiveRecord::Migration[5.1]
  def change
    change_table :payment_notifications do |t|
      t.string :request_id
    end
  end
end
