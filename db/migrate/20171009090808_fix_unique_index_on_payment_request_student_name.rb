class FixUniqueIndexOnPaymentRequestStudentName < ActiveRecord::Migration[5.1]
  def up
    remove_index :payment_requests, :student_name
    add_index :payment_requests, [:student_name, :academic_partition_id], unique: true, name: 'uniq_student_name_per_klass'
  end

  def down
    remove_index :payment_requests, [:student_name, :academic_partition_id]
    add_index :payment_requests, :student_name, unique: true
  end
end
