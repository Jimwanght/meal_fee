class AddRawContentToPaymentNotifications < ActiveRecord::Migration[5.1]
  def change
    change_table :payment_notifications do |t|
      t.text :raw_content
    end
  end
end
