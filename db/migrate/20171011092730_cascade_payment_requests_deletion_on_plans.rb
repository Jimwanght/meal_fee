class CascadePaymentRequestsDeletionOnPlans < ActiveRecord::Migration[5.1]
  def up
    remove_foreign_key :payment_requests, :plans
    add_foreign_key :payment_requests, :plans, on_delete: :cascade
  end

  def down
    remove_foreign_key :payment_requests, :plans
    add_foreign_key :payment_requests, :plans
  end
end
