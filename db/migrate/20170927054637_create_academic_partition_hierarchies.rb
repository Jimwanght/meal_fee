class CreateAcademicPartitionHierarchies < ActiveRecord::Migration[5.1]
  def change
    create_table :academic_partition_hierarchies do |t|
      t.bigint :ancestor_id, limit: 20, null: false
      t.bigint :descendant_id, limit: 20, null: false
      t.integer :distance, null: false, default: 0
    end
    add_foreign_key :academic_partition_hierarchies, :academic_partitions,
                    column: :ancestor_id,
                    on_delete: :cascade
    add_foreign_key :academic_partition_hierarchies, :academic_partitions,
                    column: :descendant_id,
                    on_delete: :cascade
    add_index :academic_partition_hierarchies, :ancestor_id
    add_index :academic_partition_hierarchies, :descendant_id

    add_index :academic_partition_hierarchies, [:ancestor_id, :descendant_id],
              unique: true,
              name: 'unique_hierarchy'
  end
end
