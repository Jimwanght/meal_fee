class AddTypeToAcademicPartitions < ActiveRecord::Migration[5.1]
  def change
    change_table :academic_partitions do |t|
      t.string :type, limit: 10, null: false

      t.index [:parent_id, :type]
    end
  end
end
