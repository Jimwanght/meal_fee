class CreateCharges < ActiveRecord::Migration[5.1]
  def change
    create_table :charges, comment: '缴费种类' do |t|
      t.string :name, limit: 30, null: false, comment: '名称'
      t.decimal :fee, precision: 10, scale: 2, null: false, default: 0, comment: '金额'
      t.belongs_to :plan, comment: '缴费计划ID'

      t.timestamps

      t.index [:name, :plan_id], unique: true
    end
    add_foreign_key :charges, :plans
  end
end
