class CascadeTargetsDeletionOnPlans < ActiveRecord::Migration[5.1]
  def up
    remove_foreign_key :targets, :plans
    add_foreign_key :targets, :plans, on_delete: :cascade
  end

  def down
    remove_foreign_key :targets, :plans
    add_foreign_key :targets, :plans
  end
end
