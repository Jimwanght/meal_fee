class LinkOrdersToCharges < ActiveRecord::Migration[5.1]
  def change
    [:orders, :order_histories].each do |table_name|
      change_table table_name do |t|
        t.belongs_to :charge, forein_key: true, comment: '收费类型ID'
      end
      add_foreign_key table_name, :charges
    end
  end
end
