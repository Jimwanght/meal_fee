class CreatePaymentNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_notifications do |t|
      t.belongs_to :order_history, forein_key: true, comment: '订单历史ID'
      t.string :payment_method, limit: 6
      t.timestamps
    end
    add_foreign_key :payment_notifications, :order_histories
  end
end
