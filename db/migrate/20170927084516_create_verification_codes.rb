class CreateVerificationCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :verification_codes do |t|
      t.string :mobile, null: false
      t.string :code, null: false
      t.datetime :expires_at, null: false
      t.string :usage, null: false
      t.index [:mobile, :usage], unique: true
      t.timestamps
    end
  end
end
