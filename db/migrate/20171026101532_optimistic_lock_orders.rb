class OptimisticLockOrders < ActiveRecord::Migration[5.1]
  def change
    change_table :orders do |t|
      t.integer :lock_version, null: false, default: 0
    end
  end
end
