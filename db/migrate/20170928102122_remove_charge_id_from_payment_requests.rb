class RemoveChargeIdFromPaymentRequests < ActiveRecord::Migration[5.1]
  def up
    remove_foreign_key :payment_requests, :charges
    remove_index :payment_requests, :charge_id
    remove_column :payment_requests, :charge_id
  end

  def down
    change_table :payment_requests do |t|
      t.belongs_to :charge, forein_key: true, comment: '收费类型ID'
    end
    add_foreign_key :payment_requests, :charges
  end
end
