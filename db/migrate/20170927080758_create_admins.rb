class CreateAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :admins, comment: '管理员' do |t|
      t.string :username, null: false, limit: 30, comment: '用户名'
      t.string :password_digest, null: false, limit: 60, comment: '加密后的密码'
      t.boolean :root, null: false, default: false, comment: '是否超级管理员'

      t.timestamps
    end
    add_index :admins, :username, unique: true
  end
end
