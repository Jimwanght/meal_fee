class CreateAcademicPartitions < ActiveRecord::Migration[5.1]
  def change
    create_table :academic_partitions, comment: '学院区划（学校、学部、年级、班级等）' do |t|
      t.string :name, comment: '名称', limit: 30
      t.bigint :parent_id, limit: 20, comment: '上级学院区划ID'

      t.timestamps
    end
    add_foreign_key :academic_partitions, :academic_partitions, column: :parent_id, on_delete: :cascade
    add_index :academic_partitions, [:name, :parent_id], unique: true
    add_index :academic_partitions, :parent_id
  end
end
