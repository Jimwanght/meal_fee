class RemoveUniqueConstraintFromPlansActive < ActiveRecord::Migration[5.1]
  def up
    remove_index :plans, :active
  end

  def down
    add_index :plans, :active, unique: true
  end
end
