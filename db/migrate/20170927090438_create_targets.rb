class CreateTargets < ActiveRecord::Migration[5.1]
  def change
    create_table :targets, id: false, comment: '缴费计划覆盖的院校' do |t|
      t.belongs_to :plan, comment: '缴费计划ID'
      t.belongs_to :academic_partition, comment: '院校ID'
      # t.bigint :college_id, limit: 20, comment: '院校ID'
      # t.foreign_key :academic_partitions, column: :college_id
      # t.index :college_id
      t.index [:academic_partition_id, :plan_id], unique: true
    end
    add_foreign_key :targets, :plans
    add_foreign_key :targets, :academic_partitions
  end
end
