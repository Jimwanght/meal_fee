class AddProtocolTitleAndProtocolContentToPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :plans, :protocol_title, :string
    add_column :plans, :protocol_content, :text
  end
end
