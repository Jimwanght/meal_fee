class CreatePlans < ActiveRecord::Migration[5.1]
  def change
    create_table :plans, comment: '缴费计划' do |t|
      t.string :name, null: false, limit: 50, comment: "计划名称"
      t.integer :payment_requests_count, default: 0, null: false, comment: "应缴人数"
      t.integer :paid_count, default: 0, null: false, comment: "实缴人数"
      t.boolean :active, null: true, comment: '是否正在缴费。是：1，否：NULL'

      t.timestamps

      t.index :active, unique: true
      t.index :name, unique: true
    end
  end
end
