class CascadeChargesDeletionOnPlans < ActiveRecord::Migration[5.1]
  def up
    remove_foreign_key :charges, :plans
    add_foreign_key :charges, :plans, on_delete: :cascade
  end

  def down
    remove_foreign_key :charges, :plans
    add_foreign_key :charges, :plans
  end
end
