class AddParentInfoToOrders < ActiveRecord::Migration[5.1]
  def change
    [:orders, :order_histories].each do |table_name|
      change_table table_name do |t|
        t.string :parent_name, limit: 50, null: false, comment: '家长姓名'
        t.string :parent_mobile, limit: 11, null: false, comment: '家长手机'
      end
    end
  end
end
