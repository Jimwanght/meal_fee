class FixUniqueIndexAgain < ActiveRecord::Migration[5.1]
  def up
    remove_index :payment_requests, [:student_name, :academic_partition_id]
    add_index :payment_requests, [:student_name, :academic_partition_id, :plan_id], unique: true, name: 'uniq_student_name'
  end

  def down
    remove_index :payment_requests, [:student_name, :academic_partition_id, :plan_id]
    add_index :payment_requests, [:student_name, :academic_partition_id], unique: true, name: 'uniq_student_name_per_klass'
  end
end
