class CreatePayers < ActiveRecord::Migration[5.1]
  def change
    create_table :payers, comment: '付款人' do |t|
      t.string :mobile, null: false, limit: 11, comment: '手机号'
      t.string :password_digest, null: false, limit: 60, comment: '加密后的密码'

      t.timestamps
    end
    add_index :payers, :mobile, unique: true
  end
end
