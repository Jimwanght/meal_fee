class CreatePaymentRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_requests do |t|

      # 我知道的最长的名字"Uvuvwevwevwe Onyetenyevwe Ugwemubwen Ossas"长度为44个字符
      t.string :student_name, null: false, limit: 50, comment: '学生姓名'
      t.belongs_to :academic_partition, comment: '学生的班级'
      t.belongs_to :plan, comment: '缴费计划ID'
      t.belongs_to :charge, null: true, comment: '缴费种类ID'

      t.timestamps

      t.index :student_name, unique: true
    end
    add_foreign_key :payment_requests, :academic_partitions
    add_foreign_key :payment_requests, :plans
    add_foreign_key :payment_requests, :charges
  end
end
