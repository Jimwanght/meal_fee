class Admin < ApplicationRecord
  has_secure_password validations: false

  validates :username, uniqueness: {message: '用户名已被使用'},
                       length: {maximum: 30, minimum: 2, message: '用户名的长度应在2至30个字符之间'}

  def self.from_token_request(request)
    username = request.params['username'] || request.params['auth']['username']
    find_by(username: username)
  end
end