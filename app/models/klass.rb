class Klass < AcademicPartition
  belongs_to :grade, foreign_key: :parent_id
  has_many :payment_requests, foreign_key: :academic_partition_id
end