class Payer < ApplicationRecord
  has_secure_password validations: false
  has_many :orders, dependent: :restrict_with_exception
  has_many :order_histories, dependent: :restrict_with_exception

  validates :mobile, presence: {message: '手机号不能为空'},
            uniqueness: {message: '手机号已被使用'},
            format: { with: /\A1\d{10}\z/, message: '手机号格式错误' }

  def self.from_token_request(request)
    mobile = request.params['mobile'] || request.params['auth']['mobile']
    find_by(mobile: mobile)
  end
end