class AcademicPartitionHierarchy < ApplicationRecord
  belongs_to :ancestor, class_name: 'AcademicPartition'
  belongs_to :descendant, class_name: 'AcademicPartition'
end
