class Grade < AcademicPartition
  belongs_to :department, foreign_key: :parent_id
  has_many :klasses, foreign_key: :parent_id
end