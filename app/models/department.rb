class Department < AcademicPartition
  belongs_to :college, foreign_key: :parent_id
  has_many :grades, foreign_key: :parent_id
end