class Charge < ApplicationRecord
  belongs_to :plan

  validates :name, length: {minimum: 2, maximum: 30, message: '名称长度应在2至30字符之间'},
                   uniqueness: { scope: :plan_id, message: '名称已被使用' }

  validates :fee, numericality: {
      greater_than_or_equal_to: 0,
      less_than: 99999999.99,
      message: '金额必须为非负数'
  }
end
