class College < AcademicPartition
  has_many :departments, foreign_key: :parent_id

  validates :parent_id, absence: true
end