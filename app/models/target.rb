class Target < ApplicationRecord
  belongs_to :plan
  belongs_to :academic_partition

  validate :academic_partition_is_college

  before_destroy :destroy_payment_requests

  private

  def academic_partition_is_college
    errors[:academic_partition] << '征收对象不是院校' unless academic_partition.is_a? College
  end

  # def destroy_payment_requests
  #   klass_ids = AcademicPartitionHierarchy.where(ancestor_id: academic_partition_id)
  #                   .where(distance: 3)
  #                   .pluck(:descendant_id)
  #   plan.payment_requests.where(klass: klass_ids).delete_all
  # end
end
