class VerificationCode < ApplicationRecord
  validates :mobile, format: {with: /\A1\d{10}\z/, message: '手机号格式错误'}
  validates :usage, inclusion: {in: ['register', 'reset_password'], message: '用途必须为 register 或 reset_password'}
end