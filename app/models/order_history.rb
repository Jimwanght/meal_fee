class OrderHistory < ApplicationRecord
  belongs_to :payer
  belongs_to :payment_request
  belongs_to :charge, optional: true
  has_one :plan, through: :payment_request
  has_many :payment_notifications
end