module Payers
  class ControllerBase < ApplicationController
    abstract!
    before_action :authenticate_payer
  end
end