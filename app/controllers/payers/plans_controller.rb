module Payers
  class PlansController < ControllerBase
    # /payer/plans
    def index
        @plans = Plan.all.includes(:charges).where(active: true)
        return head :not_found unless @plans
        render json: @plans #, serializer: PlanWithChargesSerializer
      # end
    end

    # /payer/plans/:id
    def show
      @plan = Plan.includes(:charges).where(id: params[:id]).find_by(active: true)
      render json: @plan, serializer: PlanWithChargesSerializer
    end
  end
end