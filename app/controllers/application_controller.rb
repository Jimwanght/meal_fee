class ApplicationController < ActionController::API
  include Knock::Authenticable

  delegate :t, to: I18n

  def render_page(resources, **options)
    headers['total-pages'] = resources.total_pages
    headers['total-items'] = resources.total_count
    headers['current-page'] = resources.current_page
    render options.merge(json: resources)
  end

  # def cached(key, &block)
  #   options = Rails.cache.fetch(key) do
  #     @_cache_missed = true
  #     block.call
  #   end
  #   ___render___ options unless performed?
  # end
  #
  # def expire_caches(*key_patterns)
  #   yield; key_patterns.each {|pattern| Rails.cache.delete_matched(pattern)}
  # end
  # alias expire_cache expire_caches
  #
  # alias ___render___ render
  #
  # def render(*args)
  #   return super(*args) unless @_cache_missed
  #   body = self.class.render(*args)
  #   args.extract_options!
  #       .slice(:status, :content_type)
  #       .reverse_merge!(content_type: 'application/json; charset=utf-8', body: body)
  # end
end