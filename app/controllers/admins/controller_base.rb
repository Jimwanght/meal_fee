module Admins
  class ControllerBase < ApplicationController
    abstract!
    before_action :authenticate_admin
  end
end