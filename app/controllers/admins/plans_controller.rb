module Admins
  class PlansController < ControllerBase
    using ToBoolean

    before_action :set_plan, only: [:show, :update, :destroy, :activate, :protocol]
    before_action :validate_inactiveness, only: [:destroy]

    # GET /admins/plans
    def index
      @plans = Plan.all.includes(:academic_partitions).order(active: :desc, id: :desc).page(params[:page]).per(params[:per])
      headers['active-plan-exist'] = Plan.where(active: true).exists?
      render_page @plans
    end

    # GET /admins/plans/1
    def show
      render json: @plan
    end

    # POST /admins/plans
    def create
      @plan = Plan.new(plan_params)
      colleges = College.where(id: params[:college_ids])
      colleges.each do |college|
        @plan.targets.build(academic_partition: college)
      end

      if @plan.save
        # expire_cache "plans:index" do
          render json: @plan, status: :created
        # end
      else
        render json: @plan.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /admins/plans/1
    def update
      @plan.name = params[:name]

      college_ids = params.fetch(:college_ids, []).map(&:to_i)

      @plan.update_with_colleges!(college_ids)
      # expire_cache "plans:index", "academic_partitions:index:*" do
        render json: @plan
      # end
    rescue ActiveRecord::RecordInvalid => e
      render json: e.record.errors, status: :unprocessable_entity
    rescue ActiveRecord::DeleteRestrictionError
      render json: {base: ['已有相关订单，不能删除学校']}, status: :unprocessable_entity
    end

    # PATCH/PUT /admins/plans/1/protocol
    def protocol

      if @plan.update(params[:protocol].permit(:protocol_title, :protocol_content))
      # expire_cache "plans:index", "academic_partitions:index:*" do
        render json: @plan
      end
      rescue ActiveRecord::RecordInvalid => e
        render json: e.record.errors, status: :unprocessable_entity
      rescue ActiveRecord::DeleteRestrictionError
        render json: {base: ['已有相关订单，不能删除学校']}, status: :unprocessable_entity
      # end
    end


    # PATCH /admins/plans/1/activate
    def activate
      active = params.require(:active).to_boolean
      if active && !@plan.charges.exists?
        return render json: {charges: ['没有缴费类型']}, status: :unprocessable_entity
      end

      if @plan.update(active: active ? true : nil)
        # expire_cache "plans:index" do
          head :no_content
        # end
      else
        render json: @plan.errors
      end
    end

    # DELETE /admins/plans/1
    def destroy
      if @plan.destroy
        # expire_cache "plans:index" do
          head :no_content
        # end
      else
        render json: @plan.errors, status: :unprocessable_entity
      end
    rescue ActiveRecord::StatementInvalid
      render json: {base: ['已有相关订单']}, status: :unprocessable_entity
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan
      @plan = Plan.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def plan_params
      params.permit(:name)
    end

    def validate_inactiveness
      render json: {base: ['缴费中']}, status: :unprocessable_entity if @plan.active?
    end
  end
end