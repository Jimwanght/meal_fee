module ToBoolean
  refine String do
    def to_boolean
      self == 'true'
    end
  end

  refine NilClass do
    def to_boolean
      nil
    end
  end

  refine TrueClass do
    def to_boolean
      true
    end
  end

  refine FalseClass do
    def to_boolean
      false
    end
  end

  refine Integer do
    def to_boolean
      !zero?
    end
  end
end