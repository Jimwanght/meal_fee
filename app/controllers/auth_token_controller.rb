module AuthTokenController
  def self.[](entity_klass, serializer_class)
    Class.new(Knock::AuthTokenController) do
      define_method :entity_class do
        entity_klass
      end

      define_method :create do
        headers['auth-token'] = auth_token.token
        render json: entity, serializer: serializer_class
      end

      define_method :auth_params do
        params[:auth] ||= params
        super()
      end
    end
  end
end