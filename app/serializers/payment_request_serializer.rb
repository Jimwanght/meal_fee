class PaymentRequestSerializer < ActiveModel::Serializer
  attributes :id, :student_name
  has_one :klass, serialzer: AcademicPartitionSerializer
  has_one :grade, serialzer: AcademicPartitionSerializer
  has_one :department, serialzer: AcademicPartitionSerializer
  has_one :college, serialzer: AcademicPartitionSerializer
  has_one :order, serializer: OrderBriefSerializer
end