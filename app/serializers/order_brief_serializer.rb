class OrderBriefSerializer < ActiveModel::Serializer
  attributes :id, :payment_username, :payment_method,
             :created_at, :completed_at, :url_generated_at,
             :order_num, :state,
             :parent_name, :parent_mobile,
             :credit_card_num, :credit_card_holder, :bank

  has_one :payer, serializer: PayerSerializer
  has_one :charge, serializer: ChargeSerializer
end