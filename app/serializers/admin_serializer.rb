class AdminSerializer < ActiveModel::Serializer
  attributes :id, :username, :root

  def root
    object.root?
  end
end