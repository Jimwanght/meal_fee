class AcademicPartitionTreeSerializer < ActiveModel::Serializer
  attributes :id, :name, :type
  has_many :children, each_serializer: AcademicPartitionTreeSerializer
end
