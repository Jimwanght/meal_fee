class PlanWithChargesSerializer < ActiveModel::Serializer
  attributes :id, :name, :protocol_title, :protocol_content
  has_many :charges, each_serializer: ChargeSerializer
  has_many :colleges, each_serializer: AcademicPartitionSerializer

  def colleges
    object.academic_partitions
  end
end
