class PlanSerializer < ActiveModel::Serializer
  attributes :id, :name, :payment_requests_count, :paid_count, :active, :protocol_title, :protocol_content
  has_many :colleges, each_serializer: AcademicPartitionSerializer

  def colleges
    object.academic_partitions
  end
end
