class ChargeSerializer < ActiveModel::Serializer
  attributes :id, :name, :fee
end