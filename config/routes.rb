Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope '/', defaults: {format: :json} do
    match '*all', to: proc { [204, {}, []] }, via: :options

    namespace :admins do
      resources :academic_partitions do
        get :ancestors, on: :member
      end

      resources :admins

      resources :tokens, only: :create

      resources :plans do
        patch :activate, on: :member
        patch :protocol, on: :member

        resources :charges, shallow: true

        resources :payment_requests, shallow: true do
          post :import, on: :collection
        end
      end
    end

    namespace :payers do
      # resources :plans, only: :index

      resources :plans, only:[:index, :show]

      post 'verification_codes/:mobile' => 'verification_codes#create_or_update'

      resources :school_partitions, controller: 'academic_partitions', only: :index

      resources :academic_partitions, only: :index

      resources :tokens, only: :create

      patch 'registrations' => 'registrations#update'

      resources :registrations, only: [:create]

      resources :orders, only: [:index, :show, :create, :update, :destroy] do
        patch :paid, on: :member
        patch :failed, on: :member
      end
    end

    namespace :alipay do
      resources :payment_notifications, only: [:create]
    end

    namespace :wxpay do
      resources :payment_notifications, only: [:create]
    end
  end
end
