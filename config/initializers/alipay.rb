# Rails.application.secrets.alipay.each do |key, value|
#   Alipay.public_send(:"#{key}=", value)
# end
require 'openssl'

ALIPAY_CLIENT = Alipay::Client.new(Rails.application.secrets.alipay.merge(
  app_private_key: File.read(Rails.root.join("config/secret/alipay.#{Rails.env}.key")),
  alipay_public_key: File.read(Rails.root.join("config/secret/alipay.#{Rails.env}.pub"))
))
