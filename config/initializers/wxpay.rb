Rails.application.secrets.wxpay.each do |key, value|
  WxPay.public_send(:"#{key}=", value)
end