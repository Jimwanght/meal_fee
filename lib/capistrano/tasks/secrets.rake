namespace :deploy do
  desc 'Upload sensitive files to the server'
  task :secrets => :check do
    on roles(:app) do
      rails_env = fetch(:default_env).fetch('RAILS_ENV', 'production')
      ["alipay.#{rails_env}.key", "alipay.#{rails_env}.pub"].each do |file|
        upload! "config/secret/#{file}", shared_path.join("config/secret/#{file}")
      end
    end
  end
end